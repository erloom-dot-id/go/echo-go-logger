module gitlab.com/erloom-dot-id/go/echo-go-logger

go 1.20

require (
	github.com/getsentry/sentry-go v0.18.0
	go.uber.org/zap v1.27.0
	gopkg.in/natefinch/lumberjack.v2 v2.2.1
)

require (
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/sys v0.0.0-20220928140112-f11e5e49a4ec // indirect
	golang.org/x/text v0.3.7 // indirect
)
