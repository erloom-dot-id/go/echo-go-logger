package sentry

import "github.com/getsentry/sentry-go"

type SentryClient struct {
	sentry.ClientOptions
}

func (c *SentryClient) valueDefault() {
	if c.TracesSampleRate == 0 {
		c.TracesSampleRate = 0.1
	}
}
