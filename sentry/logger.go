package sentry

import (
	"context"

	"github.com/getsentry/sentry-go"
)

func PutMessage(ctx context.Context, msg string) {
	hub := sentry.GetHubFromContext(ctx)
	if hub == nil {
		hub = sentry.CurrentHub().Clone()
		ctx = sentry.SetHubOnContext(ctx, hub)
	}

	hub.CaptureMessage(msg)
}

func PutError(ctx context.Context, err error) {
	hub := sentry.GetHubFromContext(ctx)
	if hub == nil {
		hub = sentry.CurrentHub().Clone()
		ctx = sentry.SetHubOnContext(ctx, hub)
	}

	hub.CaptureException(err)
}
