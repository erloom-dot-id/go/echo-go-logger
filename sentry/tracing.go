package sentry

import (
	"context"
	"net/http"

	"github.com/getsentry/sentry-go"
)

func InitNewSentry(c SentryClient) error {
	c.valueDefault()

	err := sentry.Init(c.ClientOptions)
	if err != nil {
		return err
	}

	return nil
}

func Recover(ctx context.Context, r interface{}) {
	hub := sentry.GetHubFromContext(ctx)
	if hub == nil {
		hub = sentry.CurrentHub().Clone()
	}

	hub.RecoverWithContext(ctx, r)
}

func StartTransaction(ctx context.Context, tran, op string) *sentry.Span {
	hub := sentry.GetHubFromContext(ctx)
	if hub == nil {
		hub = sentry.CurrentHub().Clone()
		ctx = sentry.SetHubOnContext(ctx, hub)
	}

	return sentry.StartTransaction(ctx, tran, sentry.OpName(op))
}

func StartTransactionFromRequest(ctx context.Context, tran, op string, r *http.Request) *sentry.Span {
	hub := sentry.GetHubFromContext(ctx)
	if hub == nil {
		hub = sentry.CurrentHub().Clone()
		ctx = sentry.SetHubOnContext(ctx, hub)
	}

	return sentry.StartTransaction(ctx, tran, sentry.OpName(op), sentry.ContinueFromRequest(r))
}

func StartSpan(ctx context.Context, op string) (context.Context, *sentry.Span) {
	hub := sentry.GetHubFromContext(ctx)
	if hub == nil {
		hub = sentry.CurrentHub().Clone()
		ctx = sentry.SetHubOnContext(ctx, hub)
	}

	span := sentry.StartSpan(ctx, op)
	return span.Context(), span
}
