package elk

import (
	"time"

	"go.uber.org/zap"
)

type messageLog struct {
	Timestamps time.Time   `json:"timestamps"`
	Level      string      `json:"level"`
	Message    string      `json:"message"`
	Fields     []zap.Field `json:"fields"`
}

type ElkSetup struct {
	Path          string `json:"path"`            // default on "logs/"
	ForceLocalLog string `json:"force_local_log"` // default is true
}

func (setup *ElkSetup) valueDefault() {
	if setup.Path == "" {
		setup.Path = "logs/"
	}

	if setup.ForceLocalLog == "" {
		setup.ForceLocalLog = "true"
	}
}
