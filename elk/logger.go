package elk

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"time"

	logger "gitlab.com/erloom-dot-id/go/echo-go-logger"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

var instance *zap.Logger
var forceLocalLog = ""
var logPath = ""
var l = &lumberjack.Logger{}

func Init(setup ElkSetup) {
	config := zap.NewProductionConfig()
	config.EncoderConfig.TimeKey = "@timestamp"
	config.EncoderConfig.EncodeTime = zapcore.TimeEncoderOfLayout(time.RFC3339)
	instance, _ = config.Build(zap.AddCallerSkip(1))

	setup.valueDefault()
	forceLocalLog = setup.ForceLocalLog
	logPath = setup.Path

	var timeString = time.Now().Format("2006-01-02")
	l = &lumberjack.Logger{
		Filename:   fmt.Sprintf("%sgo-%s.log", setup.Path, timeString),
		MaxSize:    50, // megabytes
		MaxBackups: 3,
		MaxAge:     14,   //days
		Compress:   true, // disabled by default
	}
	log.SetOutput(l)
}

func CommonLog(ctx context.Context, level, message string, fields ...zap.Field) {
	if reqID, ok := ctx.Value(logger.CtxRequestID).(string); ok {
		fields = append(fields, zap.String("request_id", reqID))
	}
	switch level {
	case "info":
		instance.Info(message, fields...)
	case "error":
		instance.Error(message, fields...)
	case "warn":
		instance.Warn(message, fields...)
	case "panic":
		instance.Panic(message, fields...)
	case "debug":
		instance.Debug(message, fields...)
	}

	if forceLocalLog == "true" {
		writeMessageLog(level, message, fields)
	}
}

func writeMessageLog(level string, message string, fields []zap.Field) {
	var messageLog = messageLog{
		Timestamps: time.Now(),
		Level:      level,
		Message:    message,
		Fields:     fields,
	}
	logJson, _ := json.Marshal(messageLog)
	l.Write([]byte(fmt.Sprintf("%s\n", logJson)))
}

func TrafficLogInfo(ctx context.Context, message string, fields ...zap.Field) {
	if reqID, ok := ctx.Value(logger.CtxRequestID).(string); ok {
		fields = append(fields, zap.String("request_id", reqID))
	}
	fields = append(fields, zap.String("tag", "k8s-traffic-log"))
	instance.Info(message, fields...)

	if forceLocalLog == "true" {
		var messageLog = messageLog{
			Timestamps: time.Now(),
			Level:      "info",
			Message:    message,
			Fields:     fields,
		}
		logJson, _ := json.Marshal(messageLog)

		var timeString = time.Now().Format("2006-01-02")
		l2 := &lumberjack.Logger{
			Filename:   fmt.Sprintf("%straffic-%s.log", logPath, timeString),
			MaxSize:    500, // megabytes
			MaxBackups: 3,
			MaxAge:     14,   //days
			Compress:   true, // disabled by default
		}
		l2.Write([]byte(fmt.Sprintf("%s\n", logJson)))
	}
}

func LogInfo(ctx context.Context, message string, fields ...zap.Field) {
	fields = append(fields, zap.String("tag", "k8s-log"))
	CommonLog(ctx, "info", message, fields...)
}

func LogError(ctx context.Context, message string, fields ...zap.Field) {
	fields = append(fields, zap.String("tag", "k8s-log"))
	CommonLog(ctx, "error", message, fields...)
}

func LogPanic(ctx context.Context, message string, fields ...zap.Field) {
	fields = append(fields, zap.String("tag", "k8s-log"))
	CommonLog(ctx, "panic", message, fields...)
}

func LogWarn(ctx context.Context, message string, fields ...zap.Field) {
	fields = append(fields, zap.String("tag", "k8s-log"))
	CommonLog(ctx, "warn", message, fields...)
}

func LogDebug(ctx context.Context, message string, fields ...zap.Field) {
	fields = append(fields, zap.String("tag", "k8s-log"))
	CommonLog(ctx, "debug", message, fields...)
}
