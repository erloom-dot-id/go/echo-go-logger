# Echo Go-Logger

Package Echo Go-Logger dibuat untuk membantu para developer dalam menyimpan log error, info, atau panic di dalam project Go. Ada dua package yang dapat digunakan untuk menyimpan log, yaitu package elk yang berdasarkan package [go-uber/zap](https://github.com/uber-go/zap) dan package sentry berdasarkan package [sentry-go](https://github.com/getsentry/sentry-go).

# Elk

## Installation

Untuk menginstall package elk, kalian bisa menjalankan perintah berikut di project Go kalian.

```bash
go get gitlab.com/erloom-dot-id/go/echo-go-logger/elk@latest
```

## Usage

```go
import (
	"net/http"

    "gitlab.com/erloom-dot-id/go/echo-go-logger/elk"
)

func (rw http.ResponseWriter, r *http.Request) {

    // Fungsi LogInfo akan menyimpan log kamu yang memiliki kategori InfoLevel
    elk.LogInfo(r.Context(), "Your Info Message")

    // Fungsi LogError akan menyimpan log kamu yang memiliki kategori ErrorLevel
    elk.LogError(r.Context(), "Your Error Message")

    // Fungsi LogPanic akan menyimpan log kamu yang memiliki kategori PanicLevel
    elk.LogPanic(r.Context(), "Your Panic Message")

    // Fungsi LogWarn akan menyimpan log kamu yang memiliki kategori WarnLevel
    elk.LogWarn(r.Context(), "Your Warn Message")

    // Fungsi LogDebug akan menyimpan log kamu yang memiliki kategori DebugLevel
    elk.LogDebug(r.Context(), "Your Debug Message")
}
```

# Sentry

Di dalam package ini, selain untuk menyimpan log, kalian juga bisa untuk me-trace endpoint-endpoint yang kalian buat, seberapa baiknya performance endpoint yang kamu buat, dan juga dapat me-trace query dari eloquent database yang kalian buat.

## Installation

Untuk menginstall package sentry, kalian bisa menjalankan perintah berikut di project Go kalian.

```bash
go get gitlab.com/erloom-dot-id/go/echo-go-logger/sentry@latest
```

## Usage

Sebelum menggunakan feature echo-go-logger/sentry, kalian harus menjalankan init sentry. Pastikan kalian sudah membuat project di dalam sentry dan meng-copy dsn nya. Untuk menjalankan init sentry, bisa menggunakan fungsi InitNewSentry dengan variable SentryClient.

```go
import (
    sentryecho "gitlab.com/erloom-dot-id/go/echo-go-logger/sentry"
)

func init() {
    clients := sentryecho.SentryClient{}

    clients.Dsn = "Dsn project yang ada di sentry"
    clients.EnableTracing = true
    clients.TracesSampleRate = 0.5 // default value 0.1

    err := sentryecho.InitNewSentry(clients)
    if err != nil {
        log.Prinln(err)
    }
}
```

- Dsn bisa kalian dapatkan ketika membuat project di sentry echoteam
- EnableTracing bisa kalian masukkan true jika ingin melihat performance dari endpoint yang kalian buat.
- TracesSampleRate memiliki range antara 0.0 hingga 1.0, memiliki tipe float64.

Selain tiga parameter diatas, kalian bisa menambahkan parameter lain untuk kebutuhan advance di dalam project. Kalian bisa melihat parameter lainnya di dokumentasi milik [sentry-go](https://pkg.go.dev/github.com/getsentry/sentry-go).

Setelah kalian meng-init sentry dengan sesuai, maka dapat dijalankan feature-feature untuk me-tracing dan juga meng-capture exception.

```go
import (
    "errors"
    "net/http"

    sentryecho "gitlab.com/erloom-dot-id/go/echo-go-logger/sentry"
)

func (rw http.ResponseWriter, r *http.Request) {

    // Hampir sama dengan fungsi LogInfo yang ada di Elk, hanya saja log ini akan tersimpan di sentry (menu issues).
    sentryecho.PutMessage(r.Context(), "Your Message")

    // Hampir sama dengan fungsi LogError yang ada di Elk, hanya saja log ini akan tersimpan di sentry (menu issues).
    sentryecho.PutError(r.Context(), errors.New("This Error"))
}
```

PutMessage dan PutError bisa dipakai jika ingin meng-capture message atau error secara manual. Jika di dalam project membutuhkan sebuah trigger dimana trigger ini otomatis meng-capture ketika ada error maka kalian bisa menggunakan Recover.

```go
import (
    "net/http"

    sentryecho "gitlab.com/erloom-dot-id/go/echo-go-logger/sentry"
)

func MyMiddleware(next http.Handler) http.Handler {
    return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		defer func() {
            if r := recover(); r != nil {
                sentryecho.Recover(req.Context(), r)
            }
        }
    }
}
```

Dengan memasang fungsi Recover, endppoint yang dipasangkan dengan MyMiddleware akan otomatis meng-capture error dan disimpan ke dalam sentry.

Selain untuk meng-capture error. echo-go-logger/sentry juga dapat me-tracing performance endpoint yang dibuat. Untuk dapat melakukannya, kalian dapat menggunakan fungsi StartTransaction untuk memulainya dan fungsi StartSpan untuk me-describe operasi dari bisnis logic kalian.

```go
import (
    "context"
    "net/http"

    sentryecho "gitlab.com/erloom-dot-id/go/echo-go-logger/sentry"
)

func main() {
    ctx := context.Background()

    span := sentryecho.StartTransaction(ctx, "[GET] GetAllItem", "println")
    defer span.Finish()

    ctx = span.Context()

    reqID = span.TraceID.String()    
    ctx = context.WithValue(ctx, "X-Request-ID", reqID)

    res, err := GetAllItem(ctx)
    if err != nil {
        log.Prinln(err)
        return
    }

    fmt.Println(res)
}

type Item struct {
    ID      int
    Name    string
    Price   int
}

func GetAllItem(ctx context.Context) ([]Item, error) {
    ctx, span := StartSpan(ctx, "function.GetAllItem")
    defer span.Finish()

    items, err := QueryGetAllItem(ctx)
    if err != nil {
        return items, err
    }

    return items, nil
}

func QueryGetAllItem(ctx context.Context) ([]Item, error) {
    ctx, span := StartSpan(ctx, "query.db.QueryGetAllItem")
    defer span.Finish()

    var items []Item

    err := db.Find(&items)
    if err != nil {
        return items, err
    }

    return items, nil
}
```

Finish() harus dipanggil secara manual untuk memberi tanda akhir pada setiap span dan juga transaction.