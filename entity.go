package echogologger

type CtxKey string

const CtxRequestID CtxKey = "X-Request-ID"
